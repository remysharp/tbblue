
;    ZXZVM: Z-Code interpreter for the Z80 processor
;    Copyright (C) 1998-9,2006,2016  John Elliott <seasip.webmaster@gmail.com>
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

; 2018-03-08, Garry Lancaster: Updated version to v1.13
; 2019-05-03, Garry Lancaster: Updated version to v1.14
; 2019-05-17, Garry Lancaster: Updated version to v1.15

	defb	'ZXZVM v1.15'	; << v1.15 >>
        ; TODO After updating this to v1.16, remove the temporary "a"
        ;      sub-version from nxdep.zsm (line 90)
